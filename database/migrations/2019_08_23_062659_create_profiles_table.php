<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table ->integer('user_id');
            $table ->string('cover_image');
            $table ->string('cover_audio');
            $table ->string('default_language');
            $table ->string('profile_paypal');
            $table ->string('profile_website');
            $table->mediumText('body');
            $table ->string('s3_audio_url');
            $table ->string('s3_img_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
