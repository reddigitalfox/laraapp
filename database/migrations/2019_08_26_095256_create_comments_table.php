<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->mediumText('body');
            $table ->integer('user_id');
            $table ->integer('comment_id');
            $table ->string('s3_audio_url');
            $table ->string('s3_img_url');
            $table->timestamps();
            $table -> string('cover_image');
            $table -> string('cover_audio');
            $table -> mediumText('correction');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
