<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language');
            $table->string('title');
            $table->mediumText('body');
            $table->boolean('close');
            $table ->string('s3_audio_url');
            $table ->string('s3_img_url');
            $table -> string('cover_image');
            $table ->string('cover_audio');
            $table->timestamps();
            $table -> integer('user_id');
            $table -> string('category')->nullable;
            $table -> string('answer')->nullable;
            $table -> string('detail')->nullable;
            $table ->integer('reply_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
