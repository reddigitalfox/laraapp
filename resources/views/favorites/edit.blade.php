@extends('layouts.main')


@section('content')
        
        <form action="{{action('ProfilesController@update',$profile->id)}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <h2>{{ __('messages.editProfile') }}</h2>
        <div class="form-row-create">
            <div class="form-group">
                <label for="language">{{ __('messages.motherLanguage') }}</label>
                <select name="default_language" class="custom-select d-block w-100" required>
                  <option value="{{$profile -> default_language}}">{{$profile -> default_language}}</option>
                  <option value="cantonese">cantonese</option>
                  <option value="mandarine">mandarine</option>
                  <option value="italian">italian</option>
                  <option value="japanese">japanese</option>
                  <option value="korean">korean</option>
                  <option value="french">french</option>
                  <option value="german">german</option>
                  <option value="cantonese">spanish</option>
                  <option value="irish">irish</option>
                  <option value="portuguese">portuguese</option>
                  <option value="portuguese">russian</option>
                  <option value="dutch">dutch</option>
                  
                  <option value="english">english</option>
                </select>
                <div class="invalid-feedback">
                  Please select a valid language.
                </div>
              </div>
              <div class="form-group" style="margin-top:1rem;">
                <button type="submit" class="btn btn-primary">{{ __('messages.change') }}</button>
              </div>
        </div>

          <div class="form-group">
            <label for="exampleFormControlTextarea1">{{ __('messages.body') }}</label>
            <textarea name ="body" class="form-control" id="exampleFormControlTextarea1" rows="15">{{ $profile->body }}</textarea>
          </div>
        
        
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroupFileAddon01">{{ __('messages.image') }}</span>
            </div>
            <div class="custom-file">
              <input type="file" name="cover_image" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
              <label class="custom-file-label" for="inputGroupFile01">JPEG/PNG</label>
            </div>
          </div>
        
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="inputGroupFileAddon01">{{ __('messages.audio') }}</span>
            </div>
            <div class="custom-file">
              <input type="file" name="cover_audio" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
              <label class="custom-file-label" for="inputGroupFile01">MP3, MP4 MAX:3MB</label>
            </div>
          </div>

          <div class="form-group">
            <label for="exampleFormControlInput1">{{ __('messages.website') }}</label>
            <input  name="profile_website" class="form-control" id="profile_website" value="{{$profile->profile_website}}">
          </div>

          <div class="form-group">
            <label for="exampleFormControlInput1">{{ __('messages.paypal') }}</label>
            <br>
            <span class="paypal">{{ __('messages.paypalNote') }}</span>
            <input  name="profile_paypal" class="form-control" id="profile_paypal" value="{{$profile->profile_paypal}}">
            
          </div>

         

        </form>

@endsection

