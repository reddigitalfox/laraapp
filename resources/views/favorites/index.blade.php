@extends('layouts.main')


@section('content')


<div class="center">
<h2 style="margin:1rem">{{ __('messages.favoritePosts') }}</h2>
    @if (count($post) > 0)
        <ul class="list-group">
        @foreach ($post as $each)
            <li class="list-group-item">
                <a id="left5" href="{{action('ProfilesController@show',$each->user_id)}}" >  {{$each->body}} ({{$each->title}} posted by {{$each ->user -> name}} on {{$each->created_at}}  )</a>
                <form action="{{action('FavoritesController@destroy',$each->id)}}" method="POST" >
                        @method('DELETE')
                        @csrf
                
                        <button class="badge badge-pill badge-danger tinyButton"  type="submit">{{ __('messages.delete') }}</button>
                </form>
            </li>
        @endforeach
        </ul>
    @else
    <p>Bookmark first question.</p>
    @endif
</div>
@endsection

