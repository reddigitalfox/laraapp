<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name=”description” cont="NorthwindApps, an app developer create web apps and mobile apps. There are some contents on web development and engineering field. ">

    <title>Mobile Apps development {{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/sample.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://kit.fontawesome.com/dd84097f06.js"></script>
    

    <!-- Styles https://stackoverflow.com/questions/27563580/how-to-link-favicon-icon-at-laravel -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/topPage.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('resource/iDraft.png') }}">
</head>
<body>
    <div id="app">
        @include('components.navbar')
            @include('components.alerts')
            <main class="py-4">
                @yield('content')
            </main>
    </div>
</body>
</html>
