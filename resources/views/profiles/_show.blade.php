@extends('layouts.main')

@section('content')
    <div class="center">
    <h2 style="margin:1rem">{{$profile->user->name}}</h2>
        <div class="profile-show">
                <div class="center" >
        @if ($profile->cover_image == 'noimage.png')
          @else
              
                  <img style="max-width:50%" src="{{url($profile->blade_img_url)}}">
                  {{-- <img style="max-width:50%" src="/storage/cover_images/{{$profile->cover_image}}">   --}}
                  <br>
           
          @endif
              
          @if ($profile->cover_audio == 'ABCD.mp3')
          @else
              <br>
              <audio controls>
                  <source src="{{url($profile->blade_audio_url)}}" type="audio/mpeg">
                  Your browser does not support the audio element.
              </audio>
          @endif
        </div>
          <br>
        <p >{{$profile->body}}</p>
        <div class="show-right">
        <span>Written on {{$profile->created_at}} by {{$profile->user->name}}</span>
        </div>
        @if ($profile->profile_paypal != null)
        <div class="show-right">
        <span>Paypal email address: {{$profile->profile_paypal}}</span>
        </div>
        @endif
        @if ($profile->profile_website != null)
        <div class="show-right">
        <span>Website: {{$profile->profile_website}}</span>
        </div>
        @endif

        <br>

        @if(count($post)>0)
        <table class="table">
            <tr>  
                <th>Post record</th>
                <th></th>
              </tr>   
        @foreach($post as $each)
        <tr>  
          <th><a href="{{action('PostsController@show',$each->id)}}">{{$each->title}}</a></th>
          <th class="right">posted on {{$each->updated_at}}</th>
        </tr>    
        @endforeach
        </table>
        @else
        {{-- <p>You have no posts</p> --}}
        @endif
        </div>
    </div>
@endsection

