@extends('layouts.main')


@section('content')


<div class="center">
<h2 style="margin:1rem">{{ __('messages.users') }}</h2>
    @if (count($profiles) > 0)
        <ul class="list-group">
        @foreach ($profiles as $each)
            <li class="list-group-item"><img src="{{$each->blade_img_url}}" alt="" srcset=""/>
            <a class="Padding_10" href="{{action('ProfilesController@show',$each->user_id)}}" >{{$each->name}} - {{$each->body}}</a></li>
        @endforeach
        </ul>
    @else
    <p>No profiles found</p>
    @endif
</div>
@endsection
