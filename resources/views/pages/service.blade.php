@extends('layouts.main')
@section('content')
<br/>
{{-- <h1>{{$title}}</h1> --}}

    {{-- @if (count($users) > 0)
        <ul class="list-group">
        @foreach ($users as $each)
            <li class="list-group-item">{{$each -> name}}</li>
        @endforeach
        </ul>
    @endif --}}

    <div class="form-group row">
        <div>
            <div class="card">
                <h5 style="padding-left:20px;padding-top:10px">Privacy Policy</h5>
                <div class="card-body">
                    {{ __('messages.privacyPolicy') }}
                </div>
                <h5 style="padding-left:20px">Information Collection and Use</h5>
                <div class="card-body">
                    {{ __('messages.infoCollection') }}
                </div>
                <h5 style="padding-left:20px">Log Data</h5>
                <div class="card-body">
                    {{ __('messages.logData') }}
                </div>
                <h5 style="padding-left:20px">Cookies</h5>
                <div class="card-body">
                    {{ __('messages.cookies') }}
                </div>
                <h5 style="padding-left:20px">Service Providers</h5>
                <div class="card-body">
                    {{ __('messages.serviceProviders') }}
                </div>
                <h5 style="padding-left:20px">Security</h5>
                <div class="card-body">
                    {{ __('messages.security') }}
                </div>
                <h5 style="padding-left:20px">Links to Other Sites</h5>
                <div class="card-body">
                    {{ __('messages.links') }}
                </div>
                <h5 style="padding-left:20px">Children's Privacy</h5>
                <div class="card-body">
                    {{ __('messages.children') }}
                </div>
                <h5 style="padding-left:20px">Changes to This Privacy Policy</h5>
                <div class="card-body">
                    {{ __('messages.changes') }}
                </div>
                <h5 style="padding-left:20px">Contact</h5>
                <div class="card-body">
                    {{ __('messages.contact') }}
                </div>
            </div>
        </div>
    </div>
@endsection