@extends('layouts.main')

@section('content')

@push('styles')
<link rel="stylesheet" href="{{asset('css/index.css')}}">
@endpush

<div class="center">
    <h2 style="margin:1rem;">{{ __('messages.blog') }}</h2>
    @if (count($posts) > 0)
        <ul class="list-group">
        @foreach ($posts as $post)
        <li class="list-group-item Post_Index">
                <h5><a href="{{action('PostsController@show',$post->id)}}" >{{$post->title}}</a></h5> 
                @if ($post->cover_audio == 'ABCD.mp3')
                @else
                <div class="imgandaudio">
                    <audio controls>
                        <source src="{{url($post->s3_audio_url)}}" type="audio/mp4">
                        Your browser does not support the audio element.
                    </audio>
                </div>
                @endif
                <br>
                <span>Written on {{$post->created_at}} by <a href="{{action('ProfilesController@show',$post->user ->id)}}" >{{$post->user->name}} </a></span>
            </li>
        @endforeach
        </ul>
        <!-- {{$posts->links()}} -->
    @else
            <p style="margin:16px;">No posts found</p>
    @endif
</div>
@endsection