@extends('layouts.app')
@section('content')

@push('styles')
<link rel="stylesheet" href="{{asset('css/topPage.css')}}">
@endpush
{{-- TODO Later must do translation task --}}
    {{-- <ul id="country">
        <li style="display:inline-block;"><a class="nav-link" style="padding:0"  href="{{ url('pages/cn') }}"><span class="badge badge-primary">中文</span></a></li>
        <li style="display:inline-block;"><a class="nav-link" style="padding:0" href="{{ url('pages/jp') }}"><span class="badge badge-light">日本語</span></a></li>
        <li style="display:inline-block;"><a class="nav-link" style="padding:0" href="{{ url('pages/kr') }}"><span class="badge badge-secondary">한국어</span></a></li>
        <li style="display:inline-block;"><a class="nav-link" style="padding:0" href="{{ url('pages/ru') }}"><span class="badge badge-info">Русский</span></a></li>
        <li style="display:inline-block;"><a class="nav-link" style="padding:0" href="{{ url('pages/es') }}"><span class="badge badge-success">Español</span></a></li>
        <li style="display:inline-block;"><a class="nav-link" style="padding:0" href="{{ url('pages/de') }}"><span class="badge badge-danger">Deutsch</span></a></li>
        <li style="display:inline-block;"><a class="nav-link" style="padding:0" href="{{ url('pages/it') }}"><span class="badge badge-warning">Italiano</span></a></li>
        <li style="display:inline-block;"><a class="nav-link" style="padding:0" href="{{ url('pages/fr') }}"><span class="badge badge-dark">Français</span></a></li>
    </ul> --}}
<section style="background-color: lightblue;">

      
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h2 class="display-5">{{ __('messages.welcome') }}</h2>
        <div class="topPage" >
        <span class="explain">{{ __('messages.greeting') }}</span>
        <img src="{{asset('resource/icon.png')}}" alt="coke" style="width: 20px; height: 20px; margin-top:-10px;"/>
        </div>
        <br>
      
      
    
    
    
        <div class="carousel">
            <div class="slider">
                <section id="one">
                    <a href="https://apps.apple.com/us/app/csplus/id1520017390"><img src="{{asset('resource/CSPlusiOS.png')}}" alt="" srcset=""></a>
                </section>
                <section id="two"><a href="https://apps.apple.com/us/app/csplus/id1520017390"><img src="{{asset('resource/CSPlustext.png')}}" alt="" srcset=""></a></section>
                <section id="three"><a href="https://startnewlanguagetoday.net"><img src="{{asset('resource/iDraftAd750.png')}}" alt="" srcset=""></a></section>
                <!-- <section id="three">Campaign Ad</section> -->
            </div>
            <div class="controls">
                <button class="next">next</button>
                <button class="prev">prev</button>
            </div>
        </div>

    </div>
    
  </section>
  
   @if (count($posts)>0)
    <h3 class="margin_50">Latest posts</h3>
    <ul id="latest-posts" class="list-group">
       
        @foreach ($posts as $post)
            <li class="list-group-item Post_Index">
                <h5><a href="{{action('PostsController@show',$post->id)}}" >{{$post->title}}</a></h5> 
              
                 @if ($post->cover_image != 'noimage.png')
              
                    <img style="max-width:50%" src="{{url($post->blade_img_url)}}">
              
                 @endif
                
                
               {{--<span>Written on {{$post->created_at}} by <a href="{{action('ProfilesController@show',$post->user ->id)}}" >{{$post->user->name}} </a></span>--}}
            </li>
        @endforeach
    
        </ul>
        @endif

    {{-- footer starts --}}
    <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
            {{--<img class="mb-2" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">--}}
            
          </div>
          <div class="col-6 col-md">
            <h5>Service Policy for mobile apps</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="{{asset('resource/app_policies/privacy_policy.html')}}">Simple Days</a></li>
              <li><a class="text-muted" href="{{asset('resource/app_policies/privacy_policy_dialogguy.html')}}">Dialog Guy</a></li>
              <li><a class="text-muted" href="{{asset('resource/app_policies/privacy_policy_electric_circuit_quiz.html')}}">Online Quiz</a></li>
              <li><a class="text-muted" href="{{asset('resource/app_policies/privacy_policy_goodSchedule.html')}}">Good Schedule</a></li>
              <li><a class="text-muted" href="{{asset('resource/app_policies/privacy_policy_quickcsv.html')}}">Quick Csv</a></li>
              <li><a class="text-muted" href="{{asset('resource/app_policies/privacy_policy_timer.html')}}">-Timer</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Free Resources</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">MIT License</a></li>
              
<li><a class="text-muted" href="{{asset('resource/NewMultiDirectionCollectionView1.0.6.zip')}}">Table Maker v1.0.6 sourcecode</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>About</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="https://audiofun-sapporo.com/">Websitemaster's website</a></li>
            
            </ul>
          </div>
        </div>
      </footer>
    </div>

    {{-- previous --}}
    {{-- <div class="jumbotron text-center" style="background-color:#f8fafc; background-image: url({{asset('resource/corkboard.jpg')}}); background-size: 600px;">
        <h1 style="color:aqua"></h1>
        <p style="color:azure">It's testing version, the functionality may vary in the production version.</p>
        <p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a> <a class="btn btn-success btn-lg" href="/register" role="button">Register</a> </p>

        <p><img class="img-fluid" alt="Responsive image" src="{{asset('resource/microphone.jpg')}}" alt="desert photp"></p>
        <p style="color:azure">You like a website with no annoying-ads? me too. It's clean. Isn't it?</p>
    </div> --}}
@endsection

