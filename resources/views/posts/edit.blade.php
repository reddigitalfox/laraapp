@extends('layouts.main')

@section('content')

@push('styles')
<link rel="stylesheet" href="{{asset('css/index.css')}}">
@endpush


<form action="{{action('PostsController@update',$post->id)}}" method="POST" enctype="multipart/form-data">
@method('PATCH')
@csrf
<h2>{{ __('messages.editArticle') }}</h2>
<div class="form-row-create">
<div class="form-group">
    <label for="country">{{ __('messages.language') }}</label>
    <select name="language" class="custom-select d-block w-100" required>
      <option value="{{$post -> language}}">{{$post -> language}}</option>
      <option value="blog">blog</option>
      <option value="cantonese">cantonese</option>
      <option value="mandarine">mandarine</option>
      <option value="italian">italian</option>
      <option value="japanese">japanese</option>
      <option value="korean">korean</option>
      <option value="french">french</option>
      <option value="german">german</option>
      <option value="cantonese">spanish</option>
      <option value="irish">irish</option>
      <option value="portuguese">portuguese</option>
      <option value="portuguese">russian</option>
      <option value="dutch">dutch</option>
      
      <option value="en">english</option>
    </select>
    <div class="invalid-feedback">
      Please select a valid language.
    </div>
  </div>
  <div class="form-group" style="margin-top:1rem;">
    <button type="submit" class="btn btn-primary">{{ __('messages.change') }}</button>
  </div>
</div>


<div class="form-group">
    <label for="exampleFormControlInput1">{{ __('messages.title') }}</label>
<input name="title" class="form-control" id="exampleFormControlInput1" value="{{$post->title}}">
  </div>

  <div class="form-group">
    <label for="exampleFormControlTextarea1">{{ __('messages.body') }}</label>
    <textarea name ="body" class="form-control" id="exampleFormControlTextarea1" rows="10" >{{ $post->body }}</textarea>
  </div>


  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroupFileAddon01">{{ __('messages.image') }}</span>
    </div>
    <div class="custom-file">
      <input type="file" name="cover_image" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
      <label class="custom-file-label" for="inputGroupFile01">Choose Jpeg or PNG file</label>
    </div>
  </div>

  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text" id="inputGroupFileAddon01">{{ __('messages.audio') }}</span>
    </div>
    <div class="custom-file">
      <input type="file" name="cover_audio" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
      <label class="custom-file-label" for="inputGroupFile01">Choose MP3,MP4 MAX:3MB file</label>
    </div>
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">{{ __('messages.category') }}</label>
    <input  name="category" class="form-control" id="exampleFormControlInput2" value="{{ $post->category }}" >
  </div>
  
     
    <div class="form-group">
      <label for="exampleFormControlInput1">{{ __('messages.note1') }}</label>
      <input  name="note1" class="form-control" id="exampleFormControlInput3" >
    </div>
    
    <div class="form-group">
      <label for="exampleFormControlInput1">{{ __('messages.note2') }}</label>
      <input  name="note2" class="form-control" id="exampleFormControlInput4" >
    </div>

  </form>

@endsection