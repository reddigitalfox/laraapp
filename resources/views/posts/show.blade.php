@extends('layouts.main')

@section('content')
          <h1 class="post-title">{{$post->title}}</h1>
          <span>Written on {{$post->created_at}}  by <a href="{{action('ProfilesController@show',$post->user ->id)}}" >{{$post->user->name}}</a></span>
      
          <div class="imgandaudio">
           @if ($post->cover_image != 'noimage.png')
              
                    <img style="max-width:50%" src="{{url($post->blade_img_url)}}">
              
            @endif
                
            @if ($post->cover_audio == 'ABCD.mp3')
            @else
                <div>
                <audio controls>
                    <source src="{{url($post->blade_audio_url)}}" type="audio/mp4">
                    Your browser does not support the audio element.
                </audio>
              </div>
            @endif
          </div>
  
   
          {!!$post->body!!}
          <!-- {{$post->body}} -->
          
          <br>
          <div class="row-bottons">
          <button type="button" class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">{{ __('messages.showComment') }}</button>
          <form action="{{action('PostsController@store4bookmark',$post->id)}}" method="POST" >
              @method('POST')
              @csrf
  
              <button type="submit" class="btn btn-success btn-sm">{{ __('messages.bookmark') }}</button>
          </form>
        </div>
@if ($comments)
<ul class="list-unstyled" style="border: 1px solid #ced4da; border-width: thin;border-radius: 0.25rem;">
@foreach ($comments as $comment)
<div class="media collapse"  id="collapseExample" >
        <li class="media" style="margin-left:15px;">
          <div class="Flex_Column">
            {{-- <h5 class="mt-0 mb-1">{{ $comment -> title }}</h5> --}}
            @if($comment->cover_image != 'noimage.png')
            @if(strpos($comment->cover_image, 'png') !== false || strpos($comment->cover_image, 'jpg') !== false || strpos($comment->cover_image, 'jpeg') !== false )
                <br>

                <img style="width:50%" src="{{url($comment->blade_img_url)}}">
    
            @endif
            @endif
            
            

            @if ($comment->cover_audio == 'ABCD.mp3')
            @else
                    <br>
                    <audio controls>
                        <source src="{{url($comment->blade_audio_url)}}" type="audio/mp4">
                        Your browser does not support the audio element.
                    </audio>
            @endif
            <br>
            <p>{{ $comment -> body }}</p>
            <footer class="blockquote-footer"> No. {{ $loop -> index }} 
              <a href="{{action('PostsController@edit',$post->id)}}">{{ $comment -> user -> name }}</a> 
              <cite title="Commented Date"> {{ $comment -> created_at  }}</cite>

              @if ($comment->cover_image == 'noimage.png')
              @else
                <a href="{{url($comment->blade_img_url)}}" >attatched file</a>
              @endif

              @if ($comment->cover_audio == 'ABCD.mp3')
              @else
                <a href="{{url($comment->blade_audio_url)}}" >audio</a>
              @endif

             
              <form action="{{action('PostsController@destroy4comment',$comment->body)}}" method="POST" >
                  @method('DELETE')
                  @csrf
      
                  <button type="submit" class="badge badge-danger" >{{ __('messages.delete') }}</button>
                </form>
           
              </footer>
          </div>
        </li>
  </div>

@endforeach  
</ul>  
@endif


{{-- Comment Form starts --}}

      <form action="{{action('PostsController@store4comment',$post->id)}}" method="POST" enctype="multipart/form-data">
        @method('POST')
        @csrf
            <div class="form-group">
              <label for="exampleFormControlTextarea1">{{ __('messages.leaveComment') }}</label>
              <textarea name ="body" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder=""></textarea>
            </div>
        
        
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupFileAddon01">{{ __('messages.doc') }}</span>
              </div>
              <div class="custom-file">
                <input type="file" name="cover_image" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="inputGroupFile01">DOC FILE MAX:2MB</label>
              </div>
            </div>
        
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupFileAddon01">{{ __('messages.audio') }}</span>
              </div>
              <div class="custom-file">
                <input type="file" name="cover_audio" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="inputGroupFile01">MP3,MP4 MAX:3MB</label>
              </div>
            </div>
        
              
              
            <div class="form-group" style="margin-top:1rem;">
              <button type="submit" class="btn btn-primary" style="width:100px">{{ __('messages.comment') }}</button>
            </div>
          </form>


{{-- footer starts --}}
<hr>
@if (!Auth::guest())
    @if (Auth::user() ->id == $post ->user_id )
            <div class="row-bottons">
                    {{-- <a href="{{action('PostsController@index')}}"  class="btn btn-outline-secondary">{{ __('messages.return') }}</a> --}}
               
                    <a href="{{action('PostsController@edit',$post->id)}}"  class="btn btn-outline-primary">{{ __('messages.edit') }}</a>
                    <a class="btn btn-outline-secondary" onclick='callModal()' >{{ __('messages.closeQuestion') }}</a>
            </div>

            <div class="post-show-modal">
              <span class="modal-label">{{ __('messages.areYouSure') }}</span>
              <div class="row-bottons-bottom">
                <a  onclick='dismissModal()'  class="btn btn-outline-secondary">Cancel</a>
                {{-- <form action="{{action('PostsController@destroy',$post->id)}}" method="POST" > --}}
                <form action="{{action('PostsController@close',$post->id)}}" method="POST" >
                    @method('POST')
                    @csrf
        
                    <button type="submit" class="btn btn-warning" style="margin-left:10px;" >{{ __('messages.close') }}</button>
                </form>
              </div>
            </div>
    @endif
@else
    <a href="{{action('PostsController@index')}}"  class="btn btn-outline-secondary">Cancel</a>

@endif

@endsection