@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row-bottons-bottom-start">
                        <a href="{{ url('/posts/create')}}" class="btn btn-primary">{{ __('messages.createArticle') }}</a>
                        <a class="btn btn-outline-secondary" onclick='callModal()' >{{ __('messages.quitService') }}</a>
                    </div>
                    <br>
                    @if(count($posts)>0)
                        <table class="table">
                        @foreach($posts as $post)
                        <tr>
                            <th>{{$post->title}}</th>
                            <th><a href="{{action('PostsController@edit',$post->id)}}">{{ __('messages.edit') }}</a></th>
                            <th><a onclick='callModalPost()' >{{ __('messages.delete') }}</a></th>
                        </tr>    
                        @endforeach
                        </table>
                    @else
                        {{-- <p>You have no posts</p> --}}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-show-modal">
        <span class="modal-label">{{ __('messages.areYouSureAccount') }}</span>
        <div class="row-bottons-bottom">
          <a  onclick='dismissModal()'  class="btn btn-outline-secondary">Cancel</a>
          <form action="{{action('ProfilesController@destroyProfile')}}" method="POST" >
              @method('DELETE')
              @csrf
  
              <button type="submit" class="btn btn-danger" style="margin-left:10px;" >{{ __('messages.shutdown') }}</button>
          </form>
        </div>
      </div>
      <div class="dashboard-show-modal-post">
            <span class="modal-label">{{ __('messages.areYouSure') }}</span>
            <div class="row-bottons-bottom">
              <a  onclick='dismissModalPost()'  class="btn btn-outline-secondary">Cancel</a>
              @if(count($posts)>0)
              <form action="{{action('PostsController@destroy',$post->id)}}" method="POST" >
                @method('DELETE')
                @csrf
        
                <button class="btn btn-outline-danger"  type="submit">{{ __('messages.delete') }}</button>
              </form>
              @endif
            </div>
        </div>
@endsection
