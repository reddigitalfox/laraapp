
  <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container" id="nav">
        <a class="navbar-brand" href="{{ url('pages/en') }}">
            <img src="{{asset('resource/iDraft.png')}}" alt="coke" id="coke" style="object-fit:contain;"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto" style="font-size:12px">
                
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}" style="text-align:center"><i class="fas fa-home fa-2x" title="home" ></i><span style="display:block">{{ __('messages.home') }}</span></a>
                  </li>
                <li class="nav-item">
                      <a class="nav-link" href="{{ url('/posts') }}" style="text-align:center"><i class="far fa-comments fa-2x" title="articles"></i><span style="display:block">{{ __('messages.drafts') }}</span></a>
                  </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ url('/blog') }}" style="text-align:center"><i class="far fa-newspaper fa-2x" title="articles"></i><span style="display:block">{{ __('messages.blog') }}</span></a>
              </li>
            @else
                
                
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/') }}" style="text-align:center"><i class="fas fa-home fa-2x" title="home" ></i><span style="display:block">{{ __('messages.home') }}</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/profiles/create') }}" style="text-align:center"><i class="fas fa-cog fa-2x" title="profile"></i><span style="display:block">{{ __('messages.profile') }}</span></a>
              </li>
              <li class="nav-item">
                    <a class="nav-link" href="{{ url('/favorites') }}" style="text-align:center"><i class="fas fa-bookmark fa-2x" title="users"></i><span style="display:block">{{ __('messages.favorite') }}</span></a>
                  </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/profiles') }}" style="text-align:center"><i class="fas fa-users fa-2x" title="users"></i><span style="display:block">{{ __('messages.users') }}</span></a>
              </li>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ url('/posts') }}" style="text-align:center"><i class="far fa-comments fa-2x" title="articles"></i><span style="display:block">{{ __('messages.drafts') }}</span></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ url('/blog') }}" style="text-align:center"><i class="far fa-newspaper fa-2x" title="articles"></i><span style="display:block">{{ __('messages.blog') }}</span></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ url('/posts/create') }}" style="text-align:center"><i class="fas fa-pen fa-2x" title="post"></i><span style="display:block">{{ __('messages.post') }}</span></a>
              </li>
              
              @endguest
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> {{ __('messages.login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-user-plus"></i> {{ __('messages.register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><i class="fas fa-user"></i>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{url('/dashboard')}}">Dashboard</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('messages.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
  