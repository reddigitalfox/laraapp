<?php  
    return [
        'welcome' => 'Apprendre est une joie simple.',
        'greeting' => "Trouvons des amis linguistiques et perfectionnons nos compétences linguistiques ensemble. Vous pouvez poster des articles avec un fichier audio.",
        'whatsnew' => "2020/0420 --Nouveau contenu, Typing Challenge est maintenant disponible.",
        'detail' => 'voir plus',
        'login' => "se connecter",
        'logout' => 'délogger',
        'register' => "s’enregistrer",
        'articles' => 'article',
        'audio' => 'fichier audio',
        'image' => "fichier d'image",
        'edit' => 'changement',
        'comment' => 'répondre',
        'showComment' => 'voir les commentaires',
        'delete' => 'suppression',
        'return' => 'Return',
        'users' => 'membre',
        'change' => 'changement',
        'send' => 'envoi',
        'body' => 'texte',
        'language' => 'langue',
        'motherLanguage' => 'langue maternelle',
        'title' => 'titre',
        'editArticle' => "Modification de l'article",
        'editProfile' => 'Modification du profil',
        'createArticle' => "Création d'article",
        'createProfile' => "Création de profil",
        'leaveComment' => 'formulaire de commentaire',
        'selfIntro' => 'À propos de moi',
        'create' => 'cadre'
    ];

?>