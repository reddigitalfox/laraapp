<?php  
    return [
        'welcome' => '환영',
        'greeting' => "언어 친구를 찾고 언어 기술을 함께 연마합시다. 오디오 파일로 기사를 게시 할 수 있습니다.",
        'register' => '등록',
        'articles' => '기사',
        'audio' => '오디오 파일',
        'login' => '로그인',
        'logout' => '로그 아웃',
        'image' => '이미지 파일',
        'edit' => '변경',
        'comment' => '코멘트',
        'showComment' => '답글 표시',
        'delete' => '삭제',
        'return' => '돌아가다',
        'users' => '회원',
        'change' => '변경',
        'send' => '송신',
        'body' => '본문',
        'language' => '언어',
        'motherLanguage' => '모국어',
        'title' => '타이틀',
        'editArticle' => '기사 편집',
        'editProfile' => '프로필 편집',
        'createArticle' => '기사 만들기',
        'createProfile' => '프로필 작성',
        'leaveComment' => '의견 양식',
        'selfIntro' => '나에 대해서',
        'create' => '작성'
    ];

?>