<?php  
    return [
        'welcome' => '欢迎',
        'greeting' => "让我们找到语言朋友，共同提高语言技能。您可以发布带有音频文件的文章。",
        'detail' => '阅读更多',
        'login' => '登录',
        'logout' => '登出',
        'register' => '注册',
        'articles' => '通讯',
        'audio' => '音讯文件',
        'image' => '图像档案',
        'edit' => '改质',
        'comment' => '复信',
        'showComment' => '显示评论',
        'delete' => '消掉',
        'return' => '返',
        'users' => '用户',
        'change' => '改质',
        'send' => '发送',
        'body' => '正文',
        'language' => '语言',
        'motherLanguage' => '母语',
        'title' => '标题',
        'editArticle' => '文章-变动',
        'editProfile' => '人物评论-变动',
        'createArticle' => '文章',
        'createProfile' => '人物评论',
        'leaveComment' => '评论部分',
        'selfIntro' => '自我介绍',
        'create' => '制作'
    ];

?>