<?php  
    return [
        'welcome' => 'Aprende algo nuevo para una simple alegría.',
        'greeting' => "Busquemos amigos del lenguaje y repasemos juntos la habilidad del lenguaje. Puede publicar artículos con archivo de audio.",
        'whatsnew' => "2020/0420 --Nuevo contenido, Typing Challenge ya está disponible.",
        'detail' => 'ver más',
        'login' => 'login',
        'logout' => 'cerrar sesión',
        'register' => 'registro',
        'articles' => 'artículo',
        'audio' => 'archivo de audio',
        'image' => 'archivo de imagen',
        'edit' => 'cambio',
        'comment' => 'responder',
        'showComment' => 'ver comentarios',
        'delete' => 'borrar',
        'return' => 'volver',
        'users' => 'miembro',
        'change' => 'cambio',
        'send' => 'enviar',
        'body' => 'texto',
        'language' => 'lengua',
        'motherLanguage' => 'mlengua materna',
        'title' => 'título',
        'editArticle' => 'Editando artículo',
        'editProfile' => 'Editando perfil',
        'createArticle' => 'Creando artículo',
        'createProfile' => 'Creando perfil',
        'leaveComment' => 'formulario de comentarios',
        'selfIntro' => 'sobre mi',
        'create' => 'creación'
    ];

?>