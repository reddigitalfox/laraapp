<?php  
    return [
        'welcome' => '学ぶことは単純な喜びです。',
        'greeting' => "友達を見つけて、一緒に言語スキルを磨きましょう。オーディオファイルを使用して記事を投稿できます。",
        'whatsnew' => "2020/0420 --新しいコンテンツ、タイピングチャレンジが利用可能になりました。",
        'detail' => '記事をみる',
        'login' => 'ログイン',
        'logout' => 'ログアウト',
        'register' => '新規登録',
        'articles' => '全ての記事',
        'audio' => '音声ファイル',
        'image' => '画像ファイル',
        'edit' => '編集',
        'comment' => '返信する',
        'showComment' => '返信を見る',
        'delete' => '削除',
        'return' => '前に戻る',
        'users' => '全ての会員',
        'change' => '変更する',
        'send' => '送信する',
        'body' => '本文',
        'language' => '使う言語',
        'motherLanguage' => '母国語',
        'title' => '題名',
        'editArticle' => '記事を編集する',
        'editProfile' => 'プロフィールを編集する',
        'createArticle' => '記事を作成する',
        'createProfile' => 'プロフィールを作成する',
        'leaveComment' => '返信欄',
        'selfIntro' => '自己紹介',
        'create' => '作成',
        'home' => 'トップ',
        'profile' => 'プロフィール',
        'messages' => 'メッセージ',
        'post' => '投稿',
    ];

?>