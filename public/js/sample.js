function callModal(){
    let post = document.querySelector('.post-show-modal');
    if (post) {
        post.classList.add('showup');
    }
    
    let dashboard = document.querySelector('.dashboard-show-modal')
    if (dashboard) {
        dashboard.classList.add('showup');
    }
    
}

function dismissModal(){
    let post = document.querySelector('.post-show-modal');
    if (post) {
        post.classList.remove('showup');
    }
    
    let dashboard = document.querySelector('.dashboard-show-modal')
    if (dashboard) {
        dashboard.classList.remove('showup');
    }

}

function callModalPost(){
    let dashboard = document.querySelector('.dashboard-show-modal-post')
    if (dashboard) {
        dashboard.classList.add('showup');
    }
    
}

function dismissModalPost(){
    let dashboard = document.querySelector('.dashboard-show-modal-post')
    if (dashboard) {
        dashboard.classList.remove('showup');
    }

}

//comment button an initial click
let button = document.querySelector('.btn.btn-primary.btn-sm');
if (button) {
    button.click();    
}



$('#inputGroupFile01').on('change',function(){
    //get the file name
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName);
})

$('#inputGroupFile02').on('change',function(){
    //get the file name
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName);
})



window.onload=function(){
    console.log("onloaded");
    const carousel = document.querySelector('.carousel');
    const slider = document.querySelector('.slider');
    
    const next = document.querySelector('.next');
    const prev = document.querySelector('.prev');
    let direction;
    
    next.addEventListener('click', function() {
      direction = -1;
      carousel.style.justifyContent = 'flex-start';
      slider.style.transform = 'translate(-50.0%)';  
    });
    
    prev.addEventListener('click', function() {
      if (direction === -1) {
        direction = 1;
        slider.appendChild(slider.firstElementChild);
      }
      carousel.style.justifyContent = 'flex-end';    
      slider.style.transform = 'translate(50.0%)';  
      
    });
    
    slider.addEventListener('transitionend', function() {
      // get the last element and append it to the front
      
      if (direction === 1) {
        slider.prepend(slider.lastElementChild);
      } else {
        slider.appendChild(slider.firstElementChild);
      }
      
      slider.style.transition = 'none';
      slider.style.transform = 'translate(0)';
      setTimeout(() => {
        slider.style.transition = 'all 0.5s';
      })
    }, false);


    myTimer();
    var myVar = setInterval(myTimer, 4000);

function myTimer() {
 
        direction = -1;
        carousel.style.justifyContent = 'flex-start';
        slider.style.transform = 'translate(-50.0%)';  
}
    
};