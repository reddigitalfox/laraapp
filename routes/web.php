<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@index');

Route::get('/settings', 'PagesController@settings');

Route::get('/service', 'PagesController@service');

Route::get('/blog', 'PagesController@blog');

Route::get('/users/{name}', function ($id,$name) {
    return 'This is user '.$name.' with the id of '.$id;
});

Route::get('pages/{locale}', 'PagesController@languageService');
Route::post('posts/store4comment/{id}', 'PostsController@store4comment');
Route::post('posts/store4bookmark/{id}', 'PostsController@store4bookmark');
Route::post('posts/close/{id}', 'PostsController@close');
Route::delete('posts/destroy4comment/{body}', 'PostsController@destroy4comment');
Route::delete('profiles/destroyProfile', 'ProfilesController@destroyProfile');

Route::resource('posts', 'PostsController');
Route::resource('profiles', 'ProfilesController');
Route::resource('comments', 'CommentsController');
Route::resource('favorites', 'FavoritesController');

Auth::routes(['verify' => true]);
Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('verified');
