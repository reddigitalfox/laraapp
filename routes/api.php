<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Mobile\AuthController@login');
    Route::post('signup', 'Mobile\AuthController@signup');
    Route::post('ios/signup', 'Mobile\AuthController@iosSignup');
    Route::get('signup/activate/{token}', 'Mobile\AuthController@signupActivate');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Mobile\AuthController@logout');
        // Route::get('user', 'Mobile\AuthController@user');
        Route::post('post/store', 'Mobile\mPostController@store');
        Route::delete('post/{id}', 'Mobile\mPostController@destroy');
        Route::post('comment/{id}', 'Mobile\mCommentController@store');
        Route::delete('comment/{id}', 'Mobile\mCommentController@destroy');
        Route::post('profile/store', 'Mobile\mProfileController@store');
        Route::post('post/close/{id}', 'Mobile\mPostController@close');
        Route::post('profile/{id}', 'Mobile\mProfileController@update');
        Route::delete('profile/{id}', 'Mobile\mProfileController@destroy');
        Route::delete('favorite/{id}', 'Mobile\mFavoriteController@destroy');
        Route::get('favorite/user', 'Mobile\mFavoriteController@user');
        Route::post('favorite/store/{id}', 'Mobile\mFavoriteController@store');
        Route::post('favorite/{id}', 'Mobile\mFavoriteController@update');
        Route::post('report/store/{id}', 'Mobile\mRefusedController@store');
        Route::post('post/{id}', 'Mobile\mPostController@update');

        
        Route::get('profile/user', 'Mobile\mProfileController@user');
        Route::get('post/user', 'Mobile\mPostController@user');
        
        
        Route::get('profile', 'Mobile\mProfileController@index');
        Route::get('user', 'Mobile\mUserController@index');
    });
});



Route::group([    
    'middleware' => 'api'  
], function () {    
    Route::get('post', 'Mobile\mPostController@index');
    Route::get('comment/{id}', 'Mobile\mCommentController@search');
    Route::get('post/search/{id}', 'Mobile\mPostController@search');
    Route::get('post/show/{id}', 'Mobile\mPostController@show');
    Route::get('profile/{id}', 'Mobile\mProfileController@show');
   
});

Route::group([    
    'namespace' => 'Auth',    
    'middleware' => 'api',    
    'prefix' => 'password'
], function () {    
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});