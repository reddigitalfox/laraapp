<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
    use Notifiable;
    
    //table name
    protected $table = 'posts';
    //primary key
    public $primarykey = 'id';
    //time stamps
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User');
    }
}
