<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Post;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class mCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): AnonymousResourceCollection
    {
        // Get articles
        $comments = Comment::all();

        foreach ($comments as $each) {
            if (Storage::disk('dropbox')->exists($each->cover_image) && strlen($each->cover_image) > 0) {
                $each->cover_image = Storage::disk('dropbox')->url($each->cover_image);
    
            } else {
                $each->cover_image= '';
            }
           
            if (Storage::disk('dropbox')->exists('Audios/' . $each->cover_audio) && strlen($each->cover_audio) > 0) {
                $each->cover_audio = Storage::disk('dropbox')->url('Audios/' . $each->cover_audio);
    
            } else {
                $each->cover_audio = '';
            }
        }

        // Return collection of articles as a resource
        return CommentResource::collection($comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request,[
            'correction' => 'required',
            'body' => 'required',
            'cover_image' => 'nullable|max:2000',
            'cover_audio' => 'nullable|max:3000'
        ]);


        //Check post reply_count ,id is post_id
        $post = Post::find($id);
        $post->reply_count = $post->reply_count + 1;
        $post->save();

        //Create Post
        $comment = new Comment;
        $comment->body = $request->input('body');
        $comment->correction = $request->input('correction');
        $comment->user_id = auth()->user()->id;
        $comment->cover_image = $request->input('imgfile');
        $comment->cover_audio = $request->input('audiofile');
        $comment->comment_id = $id;
        $comment->s3_audio_url = $request->input('audiofile');
        $comment->s3_img_url = $request->input('imgfile');
        $comment->save();

        //End of process
        return response()->json($comment);
    }

    /**
     * Display the search result
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search($id)
    {
        //TODO Later profile'id and user's id are different
        $comment = Comment::where('comment_id', $id) ->get();
                
        // Return collection of articles as a resource
        return CommentResource::collection($comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Create Post
        $comment = Comment::find($id);
        $comment->delete();

        return "comment delete success";
    }
}
