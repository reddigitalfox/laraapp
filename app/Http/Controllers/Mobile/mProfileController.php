<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profile;
use App\Post;
use App\Favorite;
use App\User;
use App\Comment;
use App\Http\Resources\Profile as ProfileResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class mProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): AnonymousResourceCollection
    {
        // Get articles
        $profile = Profile::all();

        // Return collection of articles as a resource
        return ProfileResource::collection($profile);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'default_language' => 'required',
            'body' => 'required|min:5',
            'cover_image' => 'image||nullable|max:2000',
            'cover_audio' => 'nullable|max:3000'
        ]);

        //Handle Imag File Upload
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            //$path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore,'s3');
            //$imgUrl = Storage::disk('s3')->url($path);
        }else{
            $fileNameToStore = 'noimage.png';
            $imgUrl = '';
        }

        //Handle Audio File Upload
        if ($request->hasFile('cover_audio') && $request->file('cover_audio')-> getMimeType () == 'audio/mpeg' || $request->hasFile('cover_audio') && $request->file('cover_audio')-> getMimeType () == 'audio/x-m4a'  ) {
            $filenameWithExt2 = $request->file('cover_audio')->getClientOriginalName();

            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);

            $extension2 = $request->file('cover_audio')->getClientOriginalExtension();

            $fileNameToStore2 = $filename2.'_'.time().'.'.$extension2;

            //$path2 = $request->file('cover_audio')->storeAs('public/cover_audios', $fileNameToStore2,'s3');
            //$audioUrl = Storage::disk('s3')->url($path2);
        }else{
            $fileNameToStore2 = 'ABCD.mp3';
            $audioUrl = '';
        }

        //Create Post
        $profile = new Profile;
        $profile->default_language = $request->input('default_language');
        $profile->body = $request->input('body');
        $profile->user_id = auth()->user()->id;
        $profile->cover_image = $fileNameToStore;
        $profile->cover_audio = $fileNameToStore2;
        $profile->profile_paypal = $request->input('profile_paypal');
        $profile->profile_website = $request->input('profile_website');
        $profile->s3_audio_url = $audioUrl;
        $profile->s3_img_url = $imgUrl;
        $profile->save();



        return response()->json($profile);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Create Post
        $profile = Profile::where('user_id', $id)->first();
        $profile->user_name = $profile->user->name;

        return response()->json($profile);
    }

    public function user()
    {
        //TODO Later profile'id and user's id are different
        $profile = Profile::where('user_id', auth()->user()->id) ->first();
        $profile->user_name = $profile->user->name;

        return response()->json($profile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'default_language' => 'required',
            'body' => 'required|min:5',
            'cover_image' => 'image||nullable|max:2000',
            'cover_audio' => 'nullable|max:3000'
        ]);

        //Handle Imag File Upload
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            //$path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore,'s3');
            //$imgUrl = Storage::disk('s3')->url($path);
        }else{
            $fileNameToStore = 'noimage.png';
            $imgUrl = '';
        }

        //Handle Audio File Upload
        if ($request->hasFile('cover_audio') && $request->file('cover_audio')-> getMimeType () == 'audio/mpeg' || $request->hasFile('cover_audio') && $request->file('cover_audio')-> getMimeType () == 'audio/x-m4a'  ) {
            $filenameWithExt2 = $request->file('cover_audio')->getClientOriginalName();

            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);

            $extension2 = $request->file('cover_audio')->getClientOriginalExtension();

            $fileNameToStore2 = $filename2.'_'.time().'.'.$extension2;

            //$path2 = $request->file('cover_audio')->storeAs('public/cover_audios', $fileNameToStore2,'s3');
            //$audioUrl = Storage::disk('s3')->url($path2);
        }else{
            $fileNameToStore2 = 'ABCD.mp3';
            $audioUrl = '';
        }

        //Create Post
        $profile = Profile::find($id);
        $profile->default_language = $request->input('default_language');
        $profile->body = $request->input('body');
        $profile->user_id = auth()->user()->id;
        $profile->cover_image = $fileNameToStore;
        $profile->cover_audio = $fileNameToStore2;

        
        $profile->profile_paypal = $request->input('profile_paypal');
        $profile->profile_website = $request->input('profile_website');
        $profile->s3_audio_url = $audioUrl;
        $profile->s3_img_url = $imgUrl;
        $profile->save();


        return response()->json($profile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Create Post
        $profile = Profile::find($id);
        if ($profile) {
            $profile->delete();
        }
        

        $currentUser = auth()->user()->id;
        if ($currentUser){
            User::find($currentUser)->delete();
        }
        Log::debug("currentUser",$currentUser);
        Log::debug("id",$id);
       


        //delete user's favorite list
        $favorite = Favorite::where('user_id', $currentUser)->get();
        if ($favorite) {
            foreach ($favorite as $each) {
                $each->delete();
            }
        }

        $post = Post::where('user_id', $currentUser)->get();
        if ($post) {
            foreach ($post as $each) {
                $each->delete();
            }
        }

        $comments = Comment::where('comment_id', $id)->get();
        
        if($comments){
            foreach ($comments as $comment) {
                $comment->delete();
            }
        }
        
      
        // $user->delete();


        return response()->json(['Delete' => 'Profile and account deleted']);
    }
}
