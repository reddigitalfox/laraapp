<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Post;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class mCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): AnonymousResourceCollection
    {
        // Get articles
        $comments = Comment::all();

        // Return collection of articles as a resource
        return CommentResource::collection($comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request,[
            'correction' => 'required',
            'body' => 'required',
            'cover_image' => 'nullable|max:2000',
            'cover_audio' => 'nullable|max:3000'
        ]);

        //Handle Imag File Upload
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            //$path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore,'s3');
            //$imgUrl = Storage::disk('s3')->url($path);

        }else{
            $fileNameToStore = 'noimage.png';
            $imgUrl = '';
        }

        //Handle Audio File Upload
        if ($request->hasFile('cover_audio') && $request->file('cover_audio')-> getMimeType () == 'audio/mpeg' || $request->hasFile('cover_audio') && $request->file('cover_audio')-> getMimeType () == 'audio/x-m4a' ) {
            $filenameWithExt2 = $request->file('cover_audio')->getClientOriginalName();

            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);

            $extension2 = $request->file('cover_audio')->getClientOriginalExtension();

            $fileNameToStore2 = $filename2.'_'.time().'.'.$extension2;

            //$path2 = $request->file('cover_audio')->storeAs('public/cover_audios', $fileNameToStore2,'s3');

            //$audioUrl = Storage::disk('s3')->url($path2);
        }else{
            $fileNameToStore2 = 'ABCD.mp3';
            $audioUrl = '';
        }

        //Check post reply_count ,id is post_id
        $post = Post::find($id);
        $post->reply_count = $post->reply_count + 1;
        $post->save();

        //Create Post
        $comment = new Comment;
        $comment->body = $request->input('body');
        $comment->correction = $request->input('correction');
        $comment->user_id = auth()->user()->id;
        $comment->cover_image = "";
        $comment->cover_audio = "";
        $comment->comment_id = $id;
        $comment->s3_audio_url = "";
        $comment->s3_img_url = "";
        $comment->save();

        //End of process
        return response()->json($comment);
    }

    /**
     * Display the search result
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search($id)
    {
        //TODO Later profile'id and user's id are different
        $comment = Comment::where('comment_id', $id) ->get();
                
        // Return collection of articles as a resource
        return CommentResource::collection($comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Create Post
        $comment = Comment::find($id);
        $comment->delete();

        return "comment delete success";
    }
}
