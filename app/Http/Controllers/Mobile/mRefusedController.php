<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profile;
use App\Post;
use App\Favorite;
use App\User;
use App\Comment;
use App\Http\Resources\Refused as RefusedResource;
use App\Refused;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class mRefusedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
        //Check post reply_count ,id is post_id
        $post = Post::find($id);
        //Checking
        $duplicate = Refused::where('disliker_id',auth()->user()->id)->where('disliked_post_id',$id)->get();

        if($duplicate->count()){
            return response()->json(['Duplicate' => 'Alredy exists']);
        }

        //Create Post
        $refused = new Refused();
        $refused->disliker_id = auth()->user()->id;
        $refused->disliked_post_id = $id;
        $refused->disliked_id = $post->user_id;
        $refused->save();

        //End of process
        return response()->json($refused);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
