<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Favorite;
use App\Post;
use App\Http\Resources\Favorite as FavoriteResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class mFavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request,[
            // 'default_language' => 'required'
        ]);

        //Check post reply_count ,id is post_id
        $post = Post::find($id);


        

        //Checking
        $duplicate = Favorite::where('user_id',auth()->user()->id)->where('bookmark_post',$id)->get();

        if($duplicate->count()){
            return response()->json(['Duplicate' => 'Alredy exists']);
        }

        //Create Post
        $favorite = new Favorite;
        $favorite->user_id = auth()->user()->id;
        $favorite->bookmark_post = $id;
        $favorite->writer_id = $post->user_id;
        $favorite->post_reply_count = $post->reply_count;
        $favorite->save();

        //End of process
        return response()->json($favorite);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    public function user():AnonymousResourceCollection
    {
         // Get articles
         $favorites = Favorite::where('user_id',auth()->user()->id)->get();

         foreach ($favorites as $each) {
            $post = Post::where('id',$each->bookmark_post)->first();
            $each->post_title = $post->title;

            if ($post->reply_count != $each->post_reply_count) {
                $each->post_uptodate = false;
            }else{
                $each->post_uptodate = true;
            }
            
         }
         // Return collection of articles as a resource
         return FavoriteResource::collection($favorites);
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check post reply_count ,id is post_id
        $favorite = Favorite::find($id);
        $post = Post::find($favorite->bookmark_post);

        //Create Post
        
        $favorite->post_reply_count = $post->reply_count;
        $favorite->save();

        //End of process
        return response()->json($favorite);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Create Post
        $favorite = Favorite::find($id);
        $favorite->delete();

        return response()->json(['Delete' => 'Item deleted']);
    }
}
