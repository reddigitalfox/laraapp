<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Post;
use App\Favorite;
use App\Comment;
use App\Http\Resources\Post as PostResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Storage;

class mPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): AnonymousResourceCollection
    {
        // Get articles
        $posts = Post::all();
        
        $sortedObj = $posts->sortByDesc('id');

        $sortedObj->values()->all();

        foreach ($sortedObj as $sorted) {
        if (Storage::disk('dropbox')->exists($sorted->cover_image) && strlen($sorted->cover_image) > 0) {
            $sorted->cover_image = Storage::disk('dropbox')->url($sorted->cover_image);

        } else {
            $sorted->cover_image= '';
        }


       
        if (Storage::disk('dropbox')->exists('Audios/' . $sorted->cover_audio) && strlen($sorted->cover_audio) > 0) {
            $sorted->cover_audio = Storage::disk('dropbox')->url('Audios/' . $sorted->cover_audio);

        } else {
            $sorted->cover_audio = 'ABCD.mp3';
$sorted->s3_audio_url = 'ABCD.mp3';
        }

    }

        // Return collection of articles as a resource
        return PostResource::collection($sortedObj);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user(): AnonymousResourceCollection
    {
        //TODO Later profile'id and user's id are different
        $post = Post::where('user_id', auth()->user()->id) ->get();
                
        // Return collection of articles as a resource
        return PostResource::collection($post);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search($id): AnonymousResourceCollection
    {
        //TODO Later profile'id and user's id are different
        $post = Post::where('user_id', $id) ->get();
                
        // Return collection of articles as a resource
        return PostResource::collection($post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'language' => 'required',
            'title' => 'required',
            'body' => 'required|min:100'
        ]);

       
        //Create Post
        $post = new Post;
        $post->language = $request->input('language');
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->close = FALSE;
        $post->s3_audio_url = $request->input('audiofile');
        $post->s3_img_url = $request->input('imgfile');
        $post ->cover_audio = $request->input('audiofile');
        $post ->cover_image = $request->input('imgfile');
        $post->reply_count = 0;
        $post->save();


        return response()->json($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get articles
        $post = Post::find($id);
        $post ->questioner =$post->user->name;
        // Return collection of articles as a resource
        return response()->json($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //
         $this->validate($request,[
            'language' => 'required',
            'title' => 'required',
            'body' => 'required|min:100'
        ]);

       
        //Create Post
        $post = Post::find($id);
        $post->language = $request->language;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->close = FALSE;
        $post->s3_audio_url = $request->input('audiofile');
        $post->s3_img_url = $request->input('imgfile');
        $post ->cover_audio = $request->input('audiofile');
        $post ->cover_image = $request->input('imgfile');
        $post->save();

        return response()->json($post);
    }


 /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function close(Request $request, $id)
    {
         //
         $this->validate($request,[
        ]);

        //Create Post
        $post = Post::find($id); 
        $post->close = true;
        $post->save();

        $myposts = Post::where('user_id', auth()->user()->id) ->get();
                
        // Return collection of articles as a resource
        return PostResource::collection($myposts);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Create Post
        $post = Post::find($id);
        $post->delete();

        //And more comments belongs to the post
        $comments = Comment::where('comment_id', $id)->get();
        
        if($comments){
            foreach ($comments as $comment) {
                $comment->delete();
            }
        }

        $favorites = Favorite::where('bookmark_post', $id)->get();
        
        if($favorites){
            foreach ($favorites as $favorite) {
                $favorite->delete();
            }
        }

        return response()->json(['Delete' => 'Post deleted']);

    }
}
