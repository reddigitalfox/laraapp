<?php

namespace App\Http\Controllers;
use App\User;
use App;
use Session;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        // echo $user_id;
        if (intval($user_id) === 54) {
            # code...
            
            return view('dashboard_guest')->with('posts',$user->posts);
        }else{
            return view('dashboard')->with('posts',$user->posts);
        }
        
    }
}
