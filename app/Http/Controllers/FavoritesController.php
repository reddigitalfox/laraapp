<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Favorite;
use Session;
use App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FavoritesController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth',['except' => ['index','show']]);
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        $favorite = Favorite::where('user_id', auth()->user()->id)->get();
        $idx = [];
        foreach ($favorite as $each) {
            array_push($idx,$each->bookmark_post);
        }

        if ($idx != null) {
            //Multiple conditions, use whereIn
            $post = Post::whereIn('id',$idx)->get();
            return view('favorites.index')->with('post',$post);  
        }
        
        return view('favorites.index')->with('post',[]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id post id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $bookmark = Favorite::where('bookmark_post', $id)->get();

        foreach ($bookmark as $each) {
             $each->delete();
        }
        
        return redirect('/favorites')->with('success', 'Bookmark Deleted');
    }
    
}
