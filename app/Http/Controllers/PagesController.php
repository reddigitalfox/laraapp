<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Post;
use App;
use Session;
use Illuminate\Support\Facades\Storage;


class PagesController extends Controller
{
    public function index(){
        Log::info("HOME");
        $lang = Session::get('lang');
        App::setLocale($lang);
        $posts = Post::all() -> take(4);
        
        foreach ($posts as $each) {
            if (Storage::disk('dropbox')->exists($each->s3_img_url) && strlen($each->s3_img_url) > 0) {
           
                $each->blade_img_url = Storage::disk('dropbox')->url($each->s3_img_url);
            }else{
                $each->blade_img_url = '';
            }

        }
        
        
        $data = array(
            'lang' => $lang,
            'greeting' => 'Check your penpals recent activities on Language Club.',
            'posts' => $posts
        );
        
        $currentUser = auth()->user();
        
        if (is_null($currentUser)){
            return view('pages.index')->with($data);
        }else{
            return view('pages.index')->with($data);
        }
       
        
    }

    public function Service(){
        $title = 'Service';
        return view('pages.service')->with('title',$title);
    }

    public function languageService($locale){
        
        Session::put('lang',$locale);
        App::setLocale($locale);
        

        $posts = Post::all() -> take(3);
        $data = array(
            'lang' => $locale,
            'greeting' => 'Check your penpals recent activities on Language Club.',
            'posts' => $posts
        );
        return view('pages.index')->with($data);
    }

    public function blog(){
        
      
        $lang = Session::get('lang');
        App::setLocale($lang);
        // $posts = Post::all();
        //return Post::where('title','Post Two')->get();
        //$posts = DB::select('SELECT'*FROM posts');
        // $posts = Post::orderBy('title','desc')->take(1)->get();
        $posts = Post::orderBy('created_at','desc')->where('language','blog')->paginate(10);

        foreach ($posts as $each) {
            # code...

            if (Storage::disk('dropbox')->exists($each->s3_img_url) && strlen($each->s3_img_url) > 0) {
                # code...
                $each->blade_img_url = Storage::disk('dropbox')->url($each->s3_img_url);
            }else{
                $each->blade_img_url = '';
            }

            if (Storage::disk('dropbox')->exists('Audios/'.$each->s3_audio_url) && strlen($each->s3_audio_url) > 0) {
                # code...
                $each->blade_audio_url = Storage::disk('dropbox')->url('Audios/'.$each->s3_audio_url);
            }else{
                $each->blade_img_url = '';
            }
            
            
        }

        Log::info('Showing posts: '.$posts);
        
        return view('pages.blog')->with('posts',$posts);
    }
}
