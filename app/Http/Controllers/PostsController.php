<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Comment;
use App\Profile;
use Illuminate\Support\Facades\Storage;
//use DB;
use Illuminate\Support\Facades\Log;
use Session;
use App;
use App\Favorite;
use App\Notifications\NewMessage;


class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','show']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        // $posts = Post::all();
        //return Post::where('title','Post Two')->get();
        //$posts = DB::select('SELECT'*FROM posts');
        // $posts = Post::orderBy('title','desc')->take(1)->get();
        $posts = Post::orderBy('created_at','desc')->where('close',0)->paginate(10);

        foreach ($posts as $each) {
            # code...

            if (Storage::disk('dropbox')->exists($each->s3_img_url) && strlen($each->s3_img_url) > 0) {
                # code...
                $each->blade_img_url = Storage::disk('dropbox')->url($each->s3_img_url);
            }else{
                $each->blade_img_url = '';
            }

            if (Storage::disk('dropbox')->exists('Audios/'.$each->s3_audio_url) && strlen($each->s3_audio_url) > 0) {
                # code...
                $each->blade_audio_url = Storage::disk('dropbox')->url('Audios/'.$each->s3_audio_url);
            }else{
                $each->blade_img_url = '';
            }
            
            
        }

        Log::info('Showing posts: '.$posts);
        
        return view('posts.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $lang = Session::get('lang');
        App::setLocale($lang);
        $profile = Profile::where('user_id', auth()->user()->id) ->first();
        
        Log::info('Showing your profile: '.$profile);
       

        if (!$profile) {
            
            return view('profiles.create');
 
        }


        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        $this->validate($request,[
            'language' => 'required',
            'title' => 'required',
            'body' => 'required|min:10',
            'cover_image' => 'image|nullable|max:2000',
            'cover_audio' => 'nullable|max:3000'
        ]);

        $imgUrl = '';
        $audioUrl = '';
        //Handle Imag File Upload
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $filename . '.' . $extension;

            $request->file('cover_image')->storeAs('', $fileNameToStore, 'dropbox');

            $imgUrl = $fileNameToStore;
        } else {
            $fileNameToStore = 'noimage.png';
            $imgUrl = $fileNameToStore;
        }

        //Handle Audio File Upload
        if ($request->hasFile('cover_audio') && $request->file('cover_audio')->getMimeType() == 'audio/mpeg' || $request->hasFile('cover_audio') && $request->file('cover_audio')->getMimeType() == 'audio/x-m4a') {
            $filenameWithExt2 = $request->file('cover_audio')->getClientOriginalName();

            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);

            $extension2 = $request->file('cover_audio')->getClientOriginalExtension();

            $fileNameToStore2 = $filename2 . '.' . $extension2;

            $request->file('cover_audio')->storeAs('Audios/', $fileNameToStore2, 'dropbox');
            $audioUrl = $fileNameToStore2;
        } else {
            $fileNameToStore2 = 'ABCD.mp3';
            $audioUrl = '';
        }

        

        //Create Post
        $post = new Post;
        $post->language = $request->language;
        
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->cover_audio = $fileNameToStore2;
        $post->close = FALSE;
        $post->s3_audio_url = $audioUrl;
        $post->s3_img_url = $imgUrl;
        $post->reply_count = 0;

        if ($request->category) {
            $post->category = $request->category;
        }

        $post->save();

        return redirect('/posts')->with('success', 'Post Created');
    }

    public function store4comment(Request $request, $id){
        $lang = Session::get('lang');
        App::setLocale($lang);

        $this->validate($request,[
            'body' => 'required',
            'cover_image' => 'nullable|max:2000',
            'cover_audio' => 'nullable|max:3000'
        ]);

        $imgUrl = '';
        $audioUrl = '';
        //Handle Imag File Upload
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $filename . '.' . $extension;

            $request->file('cover_image')->storeAs('', $fileNameToStore, 'dropbox');

            $imgUrl = $fileNameToStore;
        } else {
            $fileNameToStore = 'noimage.png';
            $imgUrl = $fileNameToStore;
        }

        //Handle Audio File Upload
        if ($request->hasFile('cover_audio') && $request->file('cover_audio')->getMimeType() == 'audio/mpeg' || $request->hasFile('cover_audio') && $request->file('cover_audio')->getMimeType() == 'audio/x-m4a') {
            $filenameWithExt2 = $request->file('cover_audio')->getClientOriginalName();

            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);

            $extension2 = $request->file('cover_audio')->getClientOriginalExtension();

            $fileNameToStore2 = $filename2 . '.' . $extension2;

            $request->file('cover_audio')->storeAs('Audios/', $fileNameToStore2, 'dropbox');
            $audioUrl = $fileNameToStore2;
        } else {
            $fileNameToStore2 = 'ABCD.mp3';
            $audioUrl = '';
        }

        //Create Post
        $comment = new Comment;
        $comment->body = $request->input('body');
        $comment->user_id = auth()->user()->id;
        $comment->cover_image = $fileNameToStore;
        $comment->cover_audio = $fileNameToStore2;
        $comment->comment_id = $id;
        $comment->s3_audio_url = $audioUrl;
        $comment->s3_img_url = $imgUrl;
        $comment->save();




        //Send nortification
        $post = Post::where('id', $id)->first();
        $user = User::where('id',$post ->user_id)->first();

        if ($post->user_id != $comment->user_id) {
            $sender_name = auth()->user()->name;
            Log::info('Other posted comment: '.$id.'and'.$post->user_id);
            $url = [ 'url' => "'/posts/'.$id",'message' => $sender_name ." commented" ." -" .$comment->body ." [ original post: " .$post->title . "]"];
            Log::info('url: '.$url['url']);
            $user->notify(new NewMessage($url));
        }

        return redirect('/posts/'.$id)->with('success', 'Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        $post = Post::find($id);
        $comments = Comment::where('comment_id', $id)->orderBy('created_at', 'ASC')->get();
        // $data = array('comments' => $comments,'post' => $user->posts);
        // return view('posts.show')->with($data);

        if (Storage::disk('dropbox')->exists($post->s3_img_url) && strlen($post->s3_img_url) > 0) {
            $post->blade_img_url = Storage::disk('dropbox')->url($post->s3_img_url);

        } else {
            $post->blade_img_url = '';
        }
       
        if (Storage::disk('dropbox')->exists('Audios/' . $post->s3_audio_url) && strlen($post->s3_audio_url) > 0) {
            $post->blade_audio_url = Storage::disk('dropbox')->url('Audios/' . $post->s3_audio_url);

        } else {
            $post->blade_audio_url = '';
        }

        foreach ($comments as $each) {

            # code...
            if (Storage::disk('dropbox')->exists($each->s3_img_url)&& strlen($each->s3_img_url) > 0) {
                $each->blade_img_url = Storage::disk('dropbox')->url($each->s3_img_url);
    
            } else {
                $each->blade_img_url = '';
            }
           
            if (Storage::disk('dropbox')->exists('Audios/'.$each->s3_audio_url) && strlen($each->s3_audio_url) > 0) {
                
                $each->blade_audio_url = Storage::disk('dropbox')->url('Audios/'.$each->s3_audio_url);
    
            } else {
                $each->blade_audio_url = '';
            }
        }
        

        return view('posts.show',compact('post','comments'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        $post = Post::find($id);

        Log::info('Edit your post: '.$post);
        // check for correct user
        if (auth()->user()->id !== $post -> user_id) {
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }
        return view('posts.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        $this->validate($request,[
            'language' => 'required',
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image||nullable|max:2000',
            'cover_audio' => 'nullable|max:3000'
        ]);

        //Create Post
        $post = Post::find($id);

        $fileNameToStore = '';
        $fileNameToStore2 = '';
        $imgUrl = $post->s3_img_url;
        $audioUrl = $post->s3_audio_url;

         //Handle File Upload
         if ($request->hasFile('cover_image')) {

              //Delete file
            if ($post->cover_image != 'nofile.png' && $imgUrl != null) {
                Storage::disk('dropbox')->delete('' . $post->cover_image);
            }

            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $filename . '.' . $extension;

            $request->file('cover_image')->storeAs('', $fileNameToStore, 'dropbox');

            $imgUrl = $fileNameToStore;
        }

        //Handle Audio File Upload
        if ($request->hasFile('cover_audio') && $request->file('cover_audio')-> getMimeType () == 'audio/mpeg' || $request->hasFile('cover_audio') && $request->file('cover_audio')-> getMimeType () == 'audio/x-m4a'  ) {

            if ($post->cover_audio != 'ABCD.mp3' && $audioUrl != null) {
                Storage::disk('dropbox')->delete('Audios/' . $post->cover_audio);
            }
            
            $filenameWithExt2 = $request->file('cover_audio')->getClientOriginalName();

            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);

            $extension2 = $request->file('cover_audio')->getClientOriginalExtension();

            $fileNameToStore2 = $filename2 . '.' . $extension2;

            $request->file('cover_audio')->storeAs('Audios/', $fileNameToStore2, 'dropbox');
            $audioUrl = $fileNameToStore2;
        }


        $post ->language = $request->language;
        $post ->title = $request->input('title');
        $post ->body = $request->input('body');
        if ($request -> hasFile('cover_image')) {
            $post ->cover_image = $fileNameToStore;
            $post->s3_img_url = $imgUrl;
        }
        if ($request -> hasFile('cover_audio')) {
            $post ->cover_audio = $fileNameToStore2;
            $post->s3_audio_url = $audioUrl;
        }

        if ($request->category) {
            $post->category = $request->category;
        }

        $post->close = FALSE;
        $post->save();

        return redirect('/posts')->with('success', 'Post Updated');

       
        // $comments = Comment::where('comment_id', $id)->orderBy('created_at', 'ASC')->get();
        // return view('posts.show',compact('post','comments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function close(Request $request, $id)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
       
        //Create Post
        $post = Post::find($id);
        $post->close = TRUE;
        $post->save();

        return redirect('/posts')->with('success', 'Post closed');
    }

    /**
     * Remove the specified resource from storage.
     * Remove an article
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);

        //Create Post
        $post = Post::find($id);

        // check for correct user
        if (auth()->user()->id !== $post ->user_id) {
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }

        if ($post ->cover_image != 'nofile.png') {
            # code...
            Storage::disk('dropbox')->delete('' . $post->cover_image);
        }

        if ($post ->cover_audio != 'ABCD.mp3') {
            # code...
            Storage::disk('dropbox')->delete('Audios/' . $post->cover_audio);
        }

        $post->delete();

        //And more comments belongs to the post
        $comments = Comment::where('comment_id', $id)->get();
        
        if($comments){
            foreach ($comments as $comment) {
                if ($comment ->cover_image != 'nofile.png') {
                    # code...
                    Storage::disk('dropbox')->delete(''.$comment ->cover_image);
                }

                if ($comment ->cover_audio != 'ABCD.mp3') {
                    # code...
                    Storage::disk('dropbox')->delete('Audios/' . $comment ->cover_audio);
                }
                Log::info('Delete the comment: '.$comment->body);
                $comment->delete();
            }
        }


        return redirect('/posts')->with('success', 'Post Deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * 
     * @param  string  $body
     * @return \Illuminate\Http\Response
     */
    public function destroy4comment($body)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        //Create Post
        $comment = Comment::where('body', $body)->first();

        Log::info('Delete the comment: '.$comment);
        // check for correct user
        if (auth()->user()->id !== $comment-> user_id) {
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }

        if ($comment ->cover_image != 'nofile.png') {
            # code...
            Storage::disk('dropbox')->delete(''.$comment ->cover_image);
        }

        if ($comment ->cover_audio != 'ABCD.mp3') {
            # code...
            Storage::disk('dropbox')->delete('Audios/' . $comment ->cover_audio);
        }

        $comment->delete();
        return redirect('/posts/'.$comment->comment_id)->with('success', 'Comment Deleted');
    }


    /**
     * Quit from this service.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAccount($id)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);

        //Create Post
        // $post = Post::find($id);
        $posts = Post::where('user_id', $id)->get();

        foreach ($posts as $post) {

            if ($post ->cover_image != 'nofile.png') {
                Storage::disk('dropbox')->delete(''.$post ->cover_image);
            }

            if ($post ->cover_audio != 'ABCD.mp3') {
                Storage::disk('dropbox')->delete('Audios/' . $post ->cover_audio);
            }

            $post->delete();

            
        }

        Log::info('Comment Remove starts: '.$id);
        //And more comments belongs to the post
        $comments = Comment::where('user_id', $id)->get();
        foreach ($comments as $comment) {
                if ($comment ->cover_image != 'nofile.png') {
                    # code...
                    Storage::disk('dropbox')->delete(''.$comment ->cover_image);
                }

                if ($comment ->cover_audio != 'ABCD.mp3') {
                    # code...
                    Storage::disk('dropbox')->delete('Audios/' . $comment ->cover_audio);
                }
                Log::info('Delete the comment: '.$comment->body);
                $comment->delete();
        }

        return redirect('/')->with('success', 'Account deleted, Thank you for using.');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store4bookmark(Request $request,$id)
    {
        $lang = Session::get('lang');
        App::setLocale($lang);

        $post = Post::where('id', $id)->first();
       
        //Create Post
        $bookmark = new Favorite();
        $bookmark->writer_id = $post->user_id;
        $bookmark->bookmark_post = $id;
        $bookmark->user_id = auth()->user()->id;

        $check = Favorite::where('user_id', $bookmark->user_id)->where('bookmark_post', $bookmark->bookmark_post)->where('writer_id', $bookmark->writer_id)->first();
        if ($check == null) {
            $bookmark->post_reply_count = 0;
            $bookmark->save();
        }
        

        return redirect('/posts/'.$id)->with('success', 'bookmark-list updated');
    }
}
// end of class
