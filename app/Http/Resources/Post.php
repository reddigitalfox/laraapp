<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'body' => $this->body,
            'close' => $this->close,
            'cover_audio' => $this->cover_audio,
            'cover_image' => $this->cover_image,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id' => $this->id,
            'language' => $this->language,
's3_audio_url' => urlencode(Storage::disk('dropbox')->url('Audios/' . $this->s3_audio_url)),
's3_img_url' => urlencode(Storage::disk('dropbox')->url($this->s3_img_url)),
          



            'title' => $this->title,
            'user_id' => $this->user_id,
            'questioner' => $this->user->name,
            'category' => $this->category
        ];
  
}

}
