<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Profile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'body' => $this->body,
            'profile_website' => $this->profile_website,
            'profile_paypal' => $this->profile_paypal,
            'cover_audio' => $this->cover_audio,
            'cover_image' => $this->cover_image,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id' => $this->id,
            'default_language' => $this->default_language,
            's3_audio_url' => $this->s3_audio_url,
            's3_image_url' => $this->s3_image_url,
            'user_id' => $this->user_id,
            'user_name' => $this->user->name
        ];
    }
}
