<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Favorite extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'user_id' => $this->user_id, 
            'bookmark_post' =>$this->bookmark_post,
            'writer_id' =>$this->writer_id ,
            'post_reply_count' =>$this->post_reply_count,
            'post_title' =>$this->post_title,
            'post_uptodate' =>$this->post_uptodate
        ];
    }
}
