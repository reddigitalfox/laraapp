<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'body' => $this->body,
            'correction' => $this->correction,
            'cover_audio' => $this->cover_audio,
            'cover_image' => $this->cover_image,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id' => $this->id,
            'comment_id' => $this->comment_id,
            's3_audio_url' => $this->s3_audio_url,
            's3_image_url' => $this->s3_image_url,
            'user_id' => $this->user_id,
            'answerer' => $this->user->name
        ];
    }
}
