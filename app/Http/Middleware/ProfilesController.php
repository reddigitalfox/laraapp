<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Profile;
use App\Favorite;
use Session;
use App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProfilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        $profiles = Profile::orderBy('created_at', 'desc')->paginate(10);

        foreach ($profiles as $each) {
            # code...
            $each_userid = $each->user_id;
            $user = User::where('id', $each_userid)->first();


            if (strlen($user["name"]) == 0) {
                $each->name = 'noname';
            } else {
                $each->name = $user["name"];
            }

            if (strlen($each->s3_img_url) > 0) {
                $each->blade_img_url = Storage::disk('dropbox')->exists($each->s3_img_url);
            }else{
                $each->blade_img_url ='';
            }
        
        }

        return view('profiles.index')->with('profiles', $profiles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lang = Session::get('lang');
        App::setLocale($lang);
        $profile = Profile::where('user_id', auth()->user()->id)->first();

        Log::info('Showing your profile: ' . $profile);


        if ($profile) {
            return view('profiles.edit')->with('profile', $profile);
        }

        return view('profiles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $lang = Session::get('lang');
        App::setLocale($lang);
        $this->validate($request, [
            'default_language' => 'required',
            'body' => 'required|min:50',
            'cover_image' => 'image||nullable|max:2000',
            'cover_audio' => 'nullable|max:3000'
        ]);

        //Handle Imag File Upload
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $filename . '.' . $extension;

            $request->file('cover_image')->storeAs('', $fileNameToStore, 'dropbox');

            $imgUrl = $fileNameToStore;
        } else {
            $fileNameToStore = 'noimage.png';
            $imgUrl = $fileNameToStore;
        }

        //Handle Audio File Upload
        if ($request->hasFile('cover_audio') && $request->file('cover_audio')->getMimeType() == 'audio/mpeg' || $request->hasFile('cover_audio') && $request->file('cover_audio')->getMimeType() == 'audio/x-m4a') {
            $filenameWithExt2 = $request->file('cover_audio')->getClientOriginalName();

            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);

            $extension2 = $request->file('cover_audio')->getClientOriginalExtension();

            $fileNameToStore2 = $filename2 . '.' . $extension2;

            $request->file('cover_audio')->storeAs('Audios', $fileNameToStore2, 'dropbox');
            $audioUrl = $fileNameToStore2;
        } else {
            $fileNameToStore2 = 'ABCD.mp3';
            $audioUrl = '';
        }

        //Create Post
        $post = new Profile;
        $post->default_language = $request->default_language;
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->cover_audio = $fileNameToStore2;

        if ($request->profile_paypal == null) {
            $request->profile_paypal = '';
        }
        if ($request->profile_website == null) {
            $request->profile_website = '';
        }

        $post->profile_paypal = $request->profile_paypal;
        $post->profile_website = $request->profile_website;
        $post->s3_audio_url = $audioUrl;


        $post->s3_img_url = $imgUrl;
        $post->save();

        return redirect('/profiles')->with('success', 'Profile Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //
        $lang = Session::get('lang');
        App::setLocale($lang);

        //TODO Later profile'id and user's id are different
        $profile = Profile::where('user_id', $id)->first();
        $user = User::find($id);

        Log::info('Showing your profile: ' . $profile);


        # code...
        
       
        if (strlen($profile->s3_img_url) > 0) {
            $profile->blade_img_url = Storage::disk('dropbox')->url($profile->s3_img_url);

        } else {
            $profile->blade_img_url = '';
        }

        if (strlen($profile->s3_audio_url) > 0) {
            $profile->blade_audio_url = Storage::disk('dropbox')->url('Audios/' . $profile->s3_audio_url);

        } else {
            $profile->blade_audio_url = '';
        }
        $data = array('profile' => $profile, 'post' => $user->posts);
        return view('profiles.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lang = Session::get('lang');
        App::setLocale($lang);
        $post = Profile::find($id);

        // check for correct user
        if (auth()->user()->id !== $post->user_id) {
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }
        return view('profiles.edit')->with('profiles', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $lang = Session::get('lang');
        App::setLocale($lang);
        $this->validate($request, [
            'default_language' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:5000',
            'cover_audio' => 'nullable|max:5000',
            'profile_website' => 'nullable'
        ]);

        $imgUrl = '';
        $audioUrl = '';
        //Handle File Upload
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $extension = $request->file('cover_image')->getClientOriginalExtension();

            $fileNameToStore = $filename . '.' . $extension;

            $request->file('cover_image')->storeAs('', $fileNameToStore, 'dropbox');

            $imgUrl = $fileNameToStore;
        } else {
            // $imgUrl = $fileNameToStore;
        }


        //Handle Audio File Upload
        if ($request->hasFile('cover_audio') && $request->file('cover_audio')->getMimeType() == 'audio/mpeg' || $request->hasFile('cover_audio') && $request->file('cover_audio')->getMimeType() == 'audio/x-m4a') {
            $filenameWithExt2 = $request->file('cover_audio')->getClientOriginalName();

            $filename2 = pathinfo($filenameWithExt2, PATHINFO_FILENAME);

            $extension2 = $request->file('cover_audio')->getClientOriginalExtension();

            $fileNameToStore2 = $filename2 . '.' . $extension2;

            $request->file('cover_audio')->storeAs('Audios', $fileNameToStore2, 'dropbox');
            $audioUrl = $fileNameToStore2;
        }

        //Create Post
        $post = Profile::find($id);

        //Delete file
        if ($post->cover_image != 'nofile.png' && $imgUrl != null) {
            Storage::disk('dropbox')->delete('' . $post->cover_image);
        }

        if ($post->cover_audio != 'ABCD.mp3' && $audioUrl != null) {
            Storage::disk('dropbox')->delete('Audios' . $post->cover_audio);
        }

        $post->default_language = $request->default_language;
        $post->body = $request->input('body');
        if ($request->hasFile('cover_image')) {
            $post->cover_image = $fileNameToStore;
            $post->s3_img_url = $imgUrl;
        }
        if ($request->hasFile('cover_audio')) {
            $post->cover_audio = $fileNameToStore2;
            $post->s3_audio_url = $audioUrl;
        }
        if ($request->profile_paypal == null) {
            $request->profile_paypal = '';
        }
        if ($request->profile_website == null) {
            $request->profile_website = '';
        }



        $post->profile_paypal = $request->profile_paypal;
        $post->profile_website = $request->profile_website;
        $post->save();

        return redirect('/profiles')->with('success', 'Profile Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyProfile()
    {
        // check for correct user
        $currentUser = auth()->user()->id;
        Log::info('You are: ' . $currentUser);
        $profile = Profile::where('user_id', $currentUser)->first();
        $user = User::where('id', $currentUser)->first();
        Log::info('Match?: ' . $user->id);

        if ($profile) {
            if ($profile->cover_image != 'nofile.png') {
                # code...
                Storage::disk('dropbox')->delete('' . $profile->cover_image);
            }

            if ($profile->cover_audio != 'ABCD.mp3') {
                # code...
                Storage::disk('dropbox')->delete('Audios' . $profile->cover_audio);
            }


            $profile->delete();
        }

        //delete user's favorite list
        $favorite = Favorite::where('user_id', $currentUser)->get();
        if ($favorite) {
            foreach ($favorite as $each) {
                $each->delete();
            }
        }

        app('App\Http\Controllers\PostsController')->destroyAccount($currentUser);
        auth()->logout();
        $user->delete();
        return redirect('/')->with('Successful shutdown', 'Thank you for using.');
    }
}
