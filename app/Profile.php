<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    //table name
    protected $table = 'profiles';
    //primary key
    public $primarykey = 'id';

    public function user(){
        return $this->belongsTo('App\User');
    }
}
