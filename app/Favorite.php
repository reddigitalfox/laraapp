<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    //
    //table name
    protected $table = 'favorites';
    //primary key
    public $primarykey = 'id';

    public function user(){
        return $this->belongsTo('App\User');
    }
}
